<%@ page language = "java" 
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List, java.util.ArrayList,java.sql.Timestamp,java.util.Date,tutorial.Panne"
%>
<%
List<Panne> liste1 = (List<Panne>) request.getAttribute("listPannes1");
List<Panne> liste1t = (List<Panne>) request.getAttribute("listPannes1t");
List<Panne> liste2 = (List<Panne>) request.getAttribute("listPannes2");
List<Panne> liste2t = (List<Panne>) request.getAttribute("listPannes2t");
List<Panne> liste3 = (List<Panne>) request.getAttribute("listPannes3");
List<Panne> liste3t = (List<Panne>) request.getAttribute("listPannes3t");
List<Panne> liste4 = (List<Panne>) request.getAttribute("listPannes4");
List<Panne> liste4t = (List<Panne>) request.getAttribute("listPannes4t");
//List<Panne> liste1 = new ArrayList<Panne>();
//List<Panne> liste2 = new ArrayList<Panne>();
//List<Panne> liste3 = new ArrayList<Panne>();
//List<Panne> liste4 = new ArrayList<Panne>();
//Timestamp date1 = new Timestamp(93000000);
//liste1.add(new Panne(1,date1,"Reseau", "Serveur","846525454542454",false));
//liste2.add(new Panne(1,date1,"Reseau", "Serveur","846525454542454",false));
//liste3.add(new Panne(1,date1,"Reseau", "Serveur","846525454542454",false));
//liste4.add(new Panne(1,date1,"Reseau", "Serveur","846525454542454",false));
int compte1 = liste1.size();
int compte2 = liste2.size();
int compte3 = liste3.size();
int compte4 = liste4.size();
%>
<html>
<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $("#detail1").click(function(){
        $("#parmin").toggle();
    });
    $("#detail1-t").click(function(){
        $("#parmintype").toggle();
    });
    $("#detail2").click(function(){
        $("#parheure").toggle();
    });
    $("#detail2-t").click(function(){
        $("#parheuretype").toggle();
    });
    $("#detail3").click(function(){
        $("#parjour").toggle();
    });
    $("#detail3-t").click(function(){
        $("#parjourtype").toggle();
    });
    $("#detail4").click(function(){
        $("#parmois").toggle();
    });
    $("#detail4-t").click(function(){
        $("#parmoistype").toggle();
    });
});
</script>
<title>Console de Monitoring</title>
<h1>Bienvenue.</h1>
</head>
<body>

<!-- premier -->
<p>Il y a eu <%= compte1 %> Pannes depuis la derniere minute.</p>
<button id="detail1">Details</button>
<div id="parmin">
<table border="1" cellpadding="5" cellspacing="0">
	<tr>
		<th>id</th>
		<th>Date</th>
		<th>Type de panne</th>
		<th>Machine</th>
		<th>Type de machine</th>
		<th>Statut</th>
	</tr>
<%	for(Panne panne:liste1){
		if (panne != null){
		int num = panne.getId();
		Date date =panne.getDate();
		String type = panne.getTypePanne();
		String machine = panne.getIdM();
		String typem = panne.getTmachine();
		String statut;
		if (panne.isPanRes()==false){
			statut="pas r�par�";
		} else {
			statut="r�par�";
		}
%>
	<tr>
		<td><%= num %></td>
		<td><%= date %></td>
		<td><%= type %></td>
		<td><%= machine %></td>
		<td><%= typem %></td>
		<td><%= statut %>
		<%if (panne.isPanRes()==false){%>>
		<form action="MainServlet" method="post">
		<input type="submit" value="<%= num %>" name="envoyer"/>
		</form>
		</td>
		</tr>
			<%} %>
		<%} %>
	<%} %>
</table>
</div>
<button id="detail1-t">Details par type</button>
<div id="parmintype">
<table border="1" cellpadding="5" cellspacing="0">
	<tr>
		<th>id</th>
		<th>Date</th>
		<th>Type de panne</th>
		<th>Machine</th>
		<th>Type de machine</th>
		<th>Statut</th>
	</tr>
<%	for(Panne panne:liste1t){
		if (panne != null){
		int num = panne.getId();
		Date date =panne.getDate();
		String type = panne.getTypePanne();
		String machine = panne.getIdM();
		String typem = panne.getTmachine();
		String statut;
		if (panne.isPanRes()==false){
			statut="pas r�par�";
		} else {
			statut="r�par�";
		}
%>
	<tr>
		<td><%= num %></td>
		<td><%= date %></td>
		<td><%= type %></td>
		<td><%= machine %></td>
		<td><%= typem %></td>
		<td><%= statut %>
		<%if (panne.isPanRes()==false){%>>
		<form action="MainServlet" method="post">
		<input type="submit" value="<%= num %>" name="envoyer"/>
		</form>
		</td>
		</tr>
			<%} %>
		<%} %>
	<%} %>
</table>
</div>

<!-- deuxieme-->
<p>Il y a eu <%= compte2 %> Pannes depuis la derniere heure.</p>
<button id="detail2">Details</button>
<div id="parheure">
<table border="1" cellpadding="5" cellspacing="0">
	<tr>
		<th>id</th>
		<th>Date</th>
		<th>Type de panne</th>
		<th>Machine</th>
		<th>Type de machine</th>
		<th>Statut</th>
	</tr>
<%
	for(Panne panne:liste2){
		int num = panne.getId();
		Date date =panne.getDate();
		String type = panne.getTypePanne();
		String machine = panne.getIdM();
		String typem = panne.getTmachine();
		String statut;
		if (panne.isPanRes()==false){
			statut="pas r�par�";
		} else {
			statut="r�par�";
		}
%>
	<tr>
		<td><%= num %></td>
		<td><%= date %></td>
		<td><%= type %></td>
		<td><%= machine %></td>
		<td><%= typem %></td>
		<td><%= statut %>
		<%if (panne.isPanRes()==false){%>
		<form action="MainServlet" method="post">
		<input type="submit" value="<%= num %>" name="envoyer"/>
		</form>
		</td>
		</tr>
		<%} %>
	<%} %>
</table>
</div>
<button id="detail2-t">Details par type</button>
<div id="parheuretype">
<table border="1" cellpadding="5" cellspacing="0">
	<tr>
		<th>id</th>
		<th>Date</th>
		<th>Type de panne</th>
		<th>Machine</th>
		<th>Type de machine</th>
		<th>Statut</th>
	</tr>
<%
	for(Panne panne:liste2t){
		int num = panne.getId();
		Date date =panne.getDate();
		String type = panne.getTypePanne();
		String machine = panne.getIdM();
		String typem = panne.getTmachine();
		String statut;
		if (panne.isPanRes()==false){
			statut="pas r�par�";
		} else {
			statut="r�par�";
		}
%>
	<tr>
		<td><%= num %></td>
		<td><%= date %></td>
		<td><%= type %></td>
		<td><%= machine %></td>
		<td><%= typem %></td>
		<td><%= statut %>
		<%if (panne.isPanRes()==false){%>
		<form action="MainServlet" method="post">
		<input type="submit" value="<%= num %>" name="envoyer"/>
		</form>
		</td>
		</tr>
		<%} %>
	<%} %>
</table>
</div>



<!-- troisi�me -->

<p>Il y a eu <%= compte3 %> Pannes depuis le dernier jour.</p>
<button id="detail3">Details</button>
<div id="parjour">
<table border="1" cellpadding="5" cellspacing="0">
	<tr>
		<th>id</th>
		<th>Date</th>
		<th>Type de panne</th>
		<th>Machine</th>
		<th>Type de machine</th>
		<th>Statut</th>
	</tr>
<%
	for(Panne panne:liste3){
		int num = panne.getId();
		Date date =panne.getDate();
		String type = panne.getTypePanne();
		String machine = panne.getIdM();
		String typem = panne.getTmachine();
		String statut;
		if (panne.isPanRes()==false){
			statut="pas r�par�";
		} else {
			statut="r�par�";
		}
%>
	<tr>
		<td><%= num %></td>
		<td><%= date %></td>
		<td><%= type %></td>
		<td><%= machine %></td>
		<td><%= typem %></td>
		<td><%= statut %>
		<%if (panne.isPanRes()==false){%>>
		<form action="MainServlet" method="post">
			<input type="submit" value="<%= num %>" name="envoyer"/>
		</form>
		</td>
		</tr>
		<%} %>
	<%} %>
</table>
</div>
<button id="detail3-t">Details par type</button>
<div id="parjourtype">
<table border="1" cellpadding="5" cellspacing="0">
	<tr>
		<th>id</th>
		<th>Date</th>
		<th>Type de panne</th>
		<th>Machine</th>
		<th>Type de machine</th>
		<th>Statut</th>
	</tr>
<%
	for(Panne panne:liste3t){
		int num = panne.getId();
		Date date =panne.getDate();
		String type = panne.getTypePanne();
		String machine = panne.getIdM();
		String typem = panne.getTmachine();
		String statut;
		if (panne.isPanRes()==false){
			statut="pas r�par�";
		} else {
			statut="r�par�";
		}
%>
	<tr>
		<td><%= num %></td>
		<td><%= date %></td>
		<td><%= type %></td>
		<td><%= machine %></td>
		<td><%= typem %></td>
		<td><%= statut %>
		<%if (panne.isPanRes()==false){%>>
		<form action="MainServlet" method="post">
			<input type="submit" value="<%= num %>" name="envoyer"/>
		</form>
		</td>
		</tr>
		<%} %>
	<%} %>
</table>
</div>

<!-- quatrieme -->

<p>Il y a eu <%= compte4 %> Pannes depuis le dernier mois.</p>
<button id="detail4">Details</button>
<div id="parmois">
	<table border="1" cellpadding="5" cellspacing="0">
		<tr>
			<th>id</th>
			<th>Date</th>
			<th>Type de panne</th>
			<th>Machine</th>
			<th>Type de machine</th>
			<th>Statut</th>
		</tr>
	<%
		for(Panne panne:liste4){
			int num = panne.getId();
			Date date =panne.getDate();
			String type = panne.getTypePanne();
			String machine = panne.getIdM();
			String typem = panne.getTmachine();
			String statut;
			if (panne.isPanRes()==false){
				statut="pas r�par�";
			} else {
				statut="r�par�";
			}
	%>
		<tr>
			<td><%= num %></td>
			<td><%= date %></td>
			<td><%= type %></td>
			<td><%= machine %></td>
			<td><%= typem %></td>
			<td><%= statut %>
			<%if (panne.isPanRes()==false){%>>
			<form action="MainServlet" method="post">
			<input type="submit" value="<%=num %>" name="envoyer"/>
			</form>
			</td>
			
			
			<%} %>
			</tr>
		<%} %>
	</table>
</div>
<button id="detail4-t">Details par type</button>
<div id="parmoistype">
	<table border="1" cellpadding="5" cellspacing="0">
		<tr>
			<th>id</th>
			<th>Date</th>
			<th>Type de panne</th>
			<th>Machine</th>
			<th>Type de machine</th>
			<th>Statut</th>
		</tr>
	<%
		for(Panne panne:liste4t){
			int num = panne.getId();
			Date date =panne.getDate();
			String type = panne.getTypePanne();
			String machine = panne.getIdM();
			String typem = panne.getTmachine();
			String statut;
			if (panne.isPanRes()==false){
				statut="pas r�par�";
			} else {
				statut="r�par�";
			}
	%>
		<tr>
			<td><%= num %></td>
			<td><%= date %></td>
			<td><%= type %></td>
			<td><%= machine %></td>
			<td><%= typem %></td>
			<td><%= statut %>
			<%if (panne.isPanRes()==false){%>>
			<form action="MainServlet" method="post">
			<input type="submit" value="<%=num %>" name="envoyer"/>
			</form>
			</td>
			
			
			<%} %>
			</tr>
		<%} %>
	</table>
</div>

</body>
<html>