package tutorial;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MainServlet extends HttpServlet {

	public MainServlet(){
		super();
	}
	

	private void doProcess(HttpServletRequest request, HttpServletResponse response) {
			
		PanneService panneService = new PanneServiceImpl();
		
		
		
		List<Panne> listPannes1 = null;
		List<Panne> listPannes1t = null;
		List<Panne> listPannes2 = null;
		List<Panne> listPannes2t = null;
		List<Panne> listPannes3 = null;
		List<Panne> listPannes3t = null;
		List<Panne> listPannes4 = null;
		List<Panne> listPannes4t = null;
		
		
		// search by minute heure jour mois 
		listPannes1 = panneService.getPannesByminute();
		listPannes1t = panneService.getPannesByminutetype();
		listPannes2 = panneService.getPannesByheure();
		listPannes2t = panneService.getPannesByheuretype();
		listPannes3 = panneService.getPannesByjour();
		listPannes3t = panneService.getPannesByjourtype();
		listPannes4 = panneService.getPannesBymois();
		listPannes4t = panneService.getPannesBymoistype();
		
		// put list in request scope to be retrieved in jsp page
		request.setAttribute("listPannes1", listPannes1);
		request.setAttribute("listPannes1t", listPannes1t);
		request.setAttribute("listPannes2", listPannes2);
		request.setAttribute("listPannes2t", listPannes2t);
		request.setAttribute("listPannes3", listPannes3);
		request.setAttribute("listPannes3t", listPannes3t);
		request.setAttribute("listPannes4", listPannes4);
		request.setAttribute("listPannes4t", listPannes4t);

		// redirect to jsp page
		String pageName = "/monitoring.jsp";
		RequestDispatcher rd = getServletContext().getRequestDispatcher(pageName);
		try {
			rd.forward(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		doProcess(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PanneService panneService = new PanneServiceImpl();
		
		
		
		if (req.getParameter("envoyer") != null){
			String id = req.getParameter("envoyer");
			panneService.repare(id);
		}
		
		// TODO Auto-generated method stub
		doProcess(req, resp);
	}
}


