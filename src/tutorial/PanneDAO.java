package tutorial;

import java.util.List;

/**
 * 
 * @author Amine
 *
 */

public interface PanneDAO {
	/**
	 * find all pannes without any criteria
	 * 
	 * @return a list of pannes objects
	 */
	public List<Panne> findByAll();

	public List<Panne> findByMinute();

	public List<Panne> findByHour();

	public List<Panne> findByDay();

	public List<Panne> findByMonth();

	public List<Panne> findByTypenMonth();

	public List<Panne> findByTypenDay();

	public List<Panne> findByTypenHour();

	public List<Panne> findByTypenMinute();
	
	public void mettreAJour(String id);

}