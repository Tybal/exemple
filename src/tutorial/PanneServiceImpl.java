package tutorial;

import tutorial.Panne;
import tutorial.PanneDAO;
import tutorial.PanneDAOImpl;
import tutorial.PanneService;

import java.util.List;

public class PanneServiceImpl implements PanneService {

	// choose the DAO data source : DB or Mock
	private PanneDAO panneDao = new PanneDAOImpl(); 


	@Override
	public List<Panne> getPannesByminute() {
		List<Panne> listPannes = panneDao.findByMinute();
		return listPannes;
	}
	
	
	public List<Panne> getPannesByminutetype() {
		List<Panne> listPannes = panneDao.findByTypenMinute();
		return listPannes;
	}

	@Override
	public List<Panne> getPannesByheure() {
		List<Panne> listPannes = panneDao.findByHour();
		return listPannes;
	}
	
	
	public List<Panne> getPannesByheuretype() {
		List<Panne> listPannes = panneDao.findByTypenHour();
		return listPannes;
	}
	
	@Override
	public List<Panne> getPannesByjour() {
		List<Panne> listPannes = panneDao.findByDay();
		return listPannes;
	}
	
	public List<Panne> getPannesByjourtype() {
		List<Panne> listPannes = panneDao.findByTypenDay();
		return listPannes;
	}
	
	@Override
	public List<Panne> getPannesBymois() {
		List<Panne> listPannes = panneDao.findByMonth();
		return listPannes;
	}
	
	public List<Panne> getPannesBymoistype() {
		List<Panne> listPannes = panneDao.findByTypenMonth();
		return listPannes;
	}
	
	public void repare(String id){
		panneDao.mettreAJour(id);
	}
}