package tutorial;

import java.util.List;

public interface PanneService {

	public List<Panne> getPannesByminute();
	
	public List<Panne> getPannesByheure();
	
	public List<Panne> getPannesByjour();
	
	public List<Panne> getPannesBymois();
	
	public void repare(String id);
	
	public List<Panne> getPannesByminutetype();
	
	public List<Panne> getPannesByheuretype();
	
	public List<Panne> getPannesByjourtype();
	
	public List<Panne> getPannesBymoistype();

}