package tutorial;

import java.sql.Timestamp;
import java.util.Date;

/**
 * 
 * @author Amine
 *
 */

public class Panne {

	// id de la panne en héxa
	private int id;

	// date de la panne
	private Timestamp date;

	// type de la panne
	private String TypePanne;

	// type de machine
	private String Tmachine;

	// id de la machine
	private String idM;

	// boolean pour savoir si une panne est résolu ou pas
	private boolean PanRes;

	public Panne() {
		// nothing to do
	}

	public Panne(int id, Timestamp date, String typePanne, String tmachine, String idM, Boolean panres) {
		super();
		this.id = id;
		this.date = date;
		TypePanne = typePanne;
		Tmachine = tmachine;
		this.idM = idM;
		this.PanRes = panres;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isPanRes() {
		return PanRes;
	}

	public void setPanRes(boolean panRes) {
		PanRes = panRes;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getTypePanne() {
		return TypePanne;
	}

	public void setTypePanne(String typePanne) {
		TypePanne = typePanne;
	}

	public String getTmachine() {
		return Tmachine;
	}

	public void setTmachine(String tmachine) {
		Tmachine = tmachine;
	}

	public String getIdM() {
		return idM;
	}

	public void setIdM(String idM) {
		this.idM = idM;
	}

}
