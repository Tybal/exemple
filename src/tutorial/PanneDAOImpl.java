package tutorial;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author Amine
 *
 */

public class PanneDAOImpl implements PanneDAO {

	/**
	 * common method used to query DB
	 * 
	 * @param query
	 *            the SQL query to use
	 * @return a list of pannes built from the SQL query
	 */
	private List<Panne> findBy(String query) {
		Connection conn = null;
		List<Panne> listPannes = new ArrayList<Panne>();
		Statement stat = null;
		ResultSet rs = null;
		try {
			conn = DBManager.getInstance().getConnection();
			if (conn != null) {
				stat = conn.createStatement();
				rs = stat.executeQuery(query);
				while (rs.next()) {
					int id = rs.getInt("id");
					Timestamp date = rs.getTimestamp("date");
					String TypePanne = rs.getString("TypePanne");
					String Tmachine = rs.getString("Tmachine");
					String idM = rs.getString("idM");
					Boolean panres = rs.getBoolean("isRep");
					listPannes.add(new Panne(id, date, TypePanne, Tmachine, idM, panres));
				}
			}
		} catch (Exception e) {
			// TODO: handle exception : main exception should thrown to servlet
			// layer to display error message
			e.printStackTrace();

		} finally {
			// always clean up all resources in finally block
			if (conn != null) {
				DBManager.getInstance().cleanup(conn, stat, rs);
			}
		}
		return listPannes;

	}



	public List<Panne> findByAll() {
		// avoid select * queries because of performance issues,
		// only query the columns you need
		return findBy("select id,date,TypePanne,idM,Tmachine,isRep from Panne ORDER BY date");
	}

	public List<Panne> findByMinute() {
		// watch out : this query is case sensitive. use upper function on last
		// minute
		//

		return findBy(
				"select id,date,TypePanne,idM,Tmachine,isRep from Panne where  date >= DATE_SUB(NOW(), INTERVAL 1 MINUTE)");

	}

	public List<Panne> findByHour() {
		// watch out : this query is case sensitive. use upper function on last
		// hour
		//

		return findBy(
				"select id,date,TypePanne,idM,Tmachine,isRep from Panne where  date >= DATE_SUB(NOW(), INTERVAL 1 HOUR)");

	}

	public List<Panne> findByDay() {
		// watch out : this query is case sensitive. use upper function on last
		// Day
		//

		return findBy(
				"select id,date,TypePanne,idM,Tmachine,isRep from Panne where  date >= DATE_SUB(NOW(), INTERVAL 1 DAY)");

	}

	public List<Panne> findByMonth() {
		// watch out : this query is case sensitive. use upper function on last
		// Month
		//

		return findBy(
				"select id,date,TypePanne,idM,Tmachine,isRep from Panne where  date >= DATE_SUB(NOW(), INTERVAL 1 MONTH)");

	}

	public List<Panne> findByTypenMonth() {
		// watch out : this query is case sensitive. use upper function on last
		// Month by type
		//

		return findBy(
				"select id,date,TypePanne,idM,Tmachine,isRep from Panne where  date >= DATE_SUB(NOW(), INTERVAL 1 MONTH) ORDER BY Tmachine");

	}

	public List<Panne> findByTypenDay() {
		// watch out : this query is case sensitive. use upper function on last
		// Day by type
		//

		return findBy(
				"select id,date,TypePanne,idM,Tmachine,isRep from Panne where  date >= DATE_SUB(NOW(), INTERVAL 1 DAY) ORDER BY Tmachine");

	}

	public List<Panne> findByTypenHour() {
		// watch out : this query is case sensitive. use upper function on last
		// hour by type
		//

		return findBy(
				"select id,date,TypePanne,idM,Tmachine,isRep from Panne where  date >= DATE_SUB(NOW(), INTERVAL 1 HOUR) ORDER BY Tmachine");

	}

	public List<Panne> findByTypenMinute() {
		// watch out : this query is case sensitive. use upper function on last
		// minute
		//

		return findBy(
				"select id,date,TypePanne,idM,Tmachine,isRep from Panne where  date >= DATE_SUB(NOW(), INTERVAL 1 MINUTE) ORDER BY Tmachine");

	}

	
	
	public void mettreAJour(String id) {
		Connection conn = null;
		List<Panne> listPannes = new ArrayList<Panne>();
		PreparedStatement stat = null;
		ResultSet rs = null;
		try {
			conn = DBManager.getInstance().getConnection();
			if (conn != null) {
				stat = conn.prepareStatement("UPDATE Panne SET isRep=true WHERE id=?;");
				stat.setString(1, id);
				int rowsUpdated = stat.executeUpdate();
			} 
		}catch (Exception e) {
				// TODO: handle exception : main exception should thrown to servlet
				// layer to display error message
				e.printStackTrace();

		} finally {
				// always clean up all resources in finally block
				if (conn != null) {
					DBManager.getInstance().cleanup(conn, stat, rs);
				}
		}
    }

}
